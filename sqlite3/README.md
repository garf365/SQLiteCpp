sqlite3
-------

"sqlite3.c" and "sqlite3.h" files from sqlite-amalgamation-3170000.zip (SQLite 3.17.0 2017-02-13)

Those files are provided for easy setup and compatibility under Windows/Linux/MacOS.
They are used by default by the CMake build.

Use -DSQLITECPP_INTERNAL_SQLITE=OFF to link against the Linux "libsqlite3-dev" package instead.

### License:

All of the code and documentation in SQLite has been dedicated to the public domain by the authors.

